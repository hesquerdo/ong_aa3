package com.Developers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import ClasesGenericas.Socio;
import ClasesGenericas.Socios;
import ClasesGenericas.Proyecto;
import ClasesGenericas.Proyectos;
import ClasesGenericas.Voluntario;
import ClasesGenericas.Voluntarios;
import DAO.DAOFactory;

import DAO.XmlSocios;

import DAO.XmlProyectos;
import DAO.XmlVoluntarios;
import MySQL.MysqlProyectos;
import MySQL.MysqlSocios;
import MySQL.MysqlVoluntarios;




@SpringBootApplication
public class AppONGApplication {
	private static Socio socio1;
	private static Socio socio2;
	private static Socio socio3;
	private static Socios socios;
	private static Voluntario voluntario1;
	private static Voluntario voluntario2;
	private static Voluntario voluntario3;
	private static Voluntarios voluntarios;
	private static Proyecto proyecto1;
	private static Proyecto proyecto2;
	private static Proyecto proyecto3;
	private static Proyectos proyectos;
	
	
	private static Socios inicializaSocios() {
		socio1 = new Socio("10/10/2020","mensual","Jose Luis","Fernandez","Calle Avila 93","joseluis@gmail.com");
		socio2 = new Socio("11/10/2020","trimestal","Francisco","Garcia","Calle Pujades 293","francisco@gmail.com");
		socio3 = new Socio("14/10/2020","anual","Antonio","Perez","Calle Taulat 29","antonio@gmail.com");
		Socios socios = new Socios();
		socios.agregarSocio(socio1);
		socios.agregarSocio(socio2);
		socios.agregarSocio(socio3);
		
		return socios;
				
	}
	
	private static Voluntarios inicializaVoluntarios() {
		voluntario1 = new Voluntario("12/05/2001","Manuel","Perez","Av Principal","manuel@gmail.com","Brasil","Internacional"); 
		voluntario2 = new Voluntario("21/07/2011","Carlos","Garcia","Av Diagonal","carlos@gmail.com","España","Nacional");
		voluntario3 = new Voluntario("23/11/2008","Veronica","Hernandez","Av Pau Casals","veronica@gmail.com","España","Nacional");
		Voluntarios voluntarios = new Voluntarios();
		voluntarios.agregarVoluntario(voluntario1);
		voluntarios.agregarVoluntario(voluntario2);
		voluntarios.agregarVoluntario(voluntario3);
		return voluntarios;
	}
	
	private static Proyectos inicializaProyectos() {
		proyecto1 = new Proyecto("Medicinas Angola","Angola","Angola","10/05/2018","10/10/2020","Cruz Roja Sudan","UE",125345);
		proyecto2 = new Proyecto("Ayuda al Desarrollo","Sudan","Gbudwe State","07/05/2019","10/10/2020","SJR Este de Africa","Diputación Barcelona",25345);
		proyecto3 = new Proyecto("Ayuda Educativa","Brasil","Rio Janeiro","10/03/2019","10/10/2020","ONG BrasilEducativo","Fundacion Pare Tarres",12534);
		
		Proyectos proyectos = new Proyectos();
		proyectos.agregarProyecto(proyecto1);
		proyectos.agregarProyecto(proyecto2);
		proyectos.agregarProyecto(proyecto3);
		return proyectos;
		
	}
	@Autowired
	private JdbcTemplate JdbcTemplate;
			
	
	public static void checkMail (String mail) throws Exception {
					
			if(voluntario1.getCorreoElectronico().length()<=4) {
				
				throw AppONGApplication.mail_erroneo();
			
				}
		
		
	};
	private static Exception mail_erroneo() {
		// TODO Auto-generated method stub
		return null;
	}
	class mail_erroneo extends RuntimeException{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public mail_erroneo() {};
		public mail_erroneo (String mensaje_error) {
			
			super(mensaje_error);
		}
	};
	
		
	public static void main(String[] args) throws Exception {
		SpringApplication.run(AppONGApplication.class, args);
		socios = inicializaSocios();
		proyectos = inicializaProyectos();
		voluntarios = inicializaVoluntarios();
		
		String mail1 = voluntario1.getCorreoElectronico();
		checkMail(mail1);	
		
		DAOFactory XmlDAOFactory = DAOFactory.getDAOFactory(DAOFactory.XML);
		XmlSocios Socios = XmlDAOFactory.getSocios();
		
		
		Socios.guardarSocios(socios);
		Socios.listarSocios();
		XmlVoluntarios Voluntarios = XmlDAOFactory.getVoluntarios();
		Voluntarios.guardarVoluntarios(voluntarios);
		Voluntarios.listarVolunatrios();
		XmlProyectos Proyectos = XmlDAOFactory.getProyectos();
		Proyectos.guardarProyectos(proyectos);
		Proyectos.listarProyectos();
		
		DAOFactory MysqlDaoFactory = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
		
		MysqlProyectos mysqlProyectos = MysqlDaoFactory.getProyectosDao();
		MysqlSocios mysqlSocios = MysqlDaoFactory.getSociosDao();
		MysqlVoluntarios mysqlVoluntarios = MysqlDaoFactory.getVoluntariosDao();
		
		mysqlProyectos.insertar(proyecto1);
		mysqlSocios.insertar(socio1);
		mysqlVoluntarios.insertar(voluntario1);
		
	
	}
	public void run (String... strings) throws Exception{
		
		 JdbcTemplate.execute("insert into proyecto (nombreProyecto) values ('prueba')");
		
	}

}




