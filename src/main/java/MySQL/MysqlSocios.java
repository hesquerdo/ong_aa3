package MySQL;

import java.io.File;

import java.util.List;
import java.util.Map;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;


import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;


import ClasesGenericas.Socio;
import ClasesGenericas.Socios;
import DAO.SociosDAO;



import Conexion.DataSourceJDBC;

public class MysqlSocios implements SociosDAO {
	
	private DataSourceJDBC mySqlDataSource = new DataSourceJDBC();
	private JdbcTemplate jdbcTemplate = new JdbcTemplate(mySqlDataSource.getDataSource());
	
	public void  insertar(Socio id)    {
		try {
	
		        File fileSocios = new File("Socios.xml");
		        JAXBContext context = JAXBContext. newInstance (Socios.class);
			    Unmarshaller unmarshaller = context.createUnmarshaller();
			    Socios socios = (Socios)unmarshaller.unmarshal(fileSocios);
				
		        
		        List <Socio>datossocios = socios.getSocios();
		        
		        for(Socio socio : datossocios) {
		        	if(id.getNumeroSocio() == socio.getNumeroSocio()) {
		        		jdbcTemplate.update("INSERT INTO socio VALUES(?,?,?,?,?,?,?)", id.getNumeroSocio(),id.getFechaIngreso(),id.getTipoCuota(),id.getNombre(),id.getApellidos(),id.getDireccion(),id.getCorreoElectronico());
		        
				System.out.println("Se ha insertado en la base de datos el socio:" +socio.getNumeroSocio());
				
		        	}
		        }
		        
		        	
		        
	
		
	
		        	}catch(DataIntegrityViolationException esql){
		System. out .println("El socio " + id.getNumeroSocio() + " ya existe.");
		}
		catch(Exception exception) {
		System. out .println("* ERROR: " + exception.getMessage() + " *");
		}
		
}

	@Override
	public void modificar(Socio id) {

		try {
			System.out.println(
					"---------- El Socio número: " + id.getNumeroSocio() + " ,va a ser modificado. ----------");

			String CNombre = jdbcTemplate.queryForObject(
					"SELECT nombre FROM socio WHERE numeroSocio = ?;", String.class,
					id.getNumeroSocio());
			String CFechaIngreso = jdbcTemplate.queryForObject("SELECT fechaIngreso FROM socio WHERE numeroSocio = ?;",
					String.class, id.getNumeroSocio());
			String CTipoCuota = jdbcTemplate.queryForObject(
					"SELECT tipoCuota FROM socio WHERE numeroSocio = ?;", String.class,
					id.getNumeroSocio());
			String CApellidos = jdbcTemplate.queryForObject(
					"SELECT apellidos FROM socio WHERE numeroSocio = ?;", String.class,
					id.getNumeroSocio());
			String CDireccion = jdbcTemplate.queryForObject(
					"SELECT direccion FROM socio WHERE numeroSocio = ?;", String.class,
					id.getNumeroSocio());
			String CCorreoElectronico = jdbcTemplate.queryForObject(
					"SELECT correoelectronico FROM socio WHERE numeroSocio = ?;", String.class, id.getNumeroSocio());
		

			if (!id.getNombre().equals(CNombre)) {
				jdbcTemplate.update("UPDATE socio SET nombre = ? WHERE numeroSocio = ?;",
						id.getNombre(), id.getNumeroSocio());
				System.out.println("---------- El Socio número: " + id.getNumeroSocio()
						+ " ha cambiado de nombre a: " + id.getNombre() + ". ----------");

			}

			if (!id.getFechaIngreso().equals(CFechaIngreso)) {
				jdbcTemplate.update("UPDATE socio SET fechaIngreso = ? WHERE numeroSocio = ?;", id.getFechaIngreso(),
						id.getNumeroSocio());
				System.out.println("---------- El la fecha de ingreso del socio " + id.getNumeroSocio() + " ha cambiado a: "
						+ id.getFechaIngreso() + ". ----------");
			}

			if (!id.getTipoCuota().equals(CTipoCuota)) {
				jdbcTemplate.update("UPDATE socio SET tipoCuota = ? WHERE numeroSocio = ?;",
						id.getTipoCuota(), id.getNumeroSocio());
				System.out
						.println("---------- La cutoa ha cambiado a: " + id.getTipoCuota() + ". ----------");
			}

			if (!id.getApellidos().equals(CApellidos)) {
				jdbcTemplate.update("UPDATE socio SET apellidos = ? WHERE numeroSocio = ?;",
						id.getApellidos(), id.getNumeroSocio());
				System.out.println("---------- Los apellidos han cambiado a: " + id.getApellidos()
						+ ". ----------");
			}

			if (!id.getDireccion().equals(CDireccion)) {
				jdbcTemplate.update("UPDATE socio SET direccion = ? WHERE numeroSocio = ?;",
						id.getDireccion(), id.getNumeroSocio());
				System.out.println("---------- La direccion ha sido cambiada a : "
						+ id.getDireccion() + ". ----------");
			}

			if (!id.getCorreoElectronico().equals(CCorreoElectronico)) {
				jdbcTemplate.update("UPDATE proyectos SET socioLocal = ? WHERE numeroSocio = ?;", id.getCorreoElectronico(),
						id.getNumeroSocio());
				System.out.println("---------- El correo electronico ha cambiado a: " + id.getCorreoElectronico() + ". ----------");
			}

			
			System.out.println("---------- El Socio " + id.getNumeroSocio() + " ha sido modificado.---------\n ");

		} catch (DataIntegrityViolationException esql) {
			System.out.println("El socio " + id.getNumeroSocio() + " ya existe.");
		} catch (Exception exception) {
			System.out.println("* ERROR: " + exception.getMessage() + " *");
		}

	}

	public void eliminar(Socio id) {
		
		jdbcTemplate.update("DELETE FROM  socio  WHERE numeroSocio	= ?" ,id.getNumeroSocio());
        System. out .println("---------- El Socio " + id.getNumeroSocio()+ " se ha eliminado correctamente");
		
	}
	public void obtenerTodos() {
		
		List<Map<String, Object>> rows = (List<Map<String, Object>>)
				jdbcTemplate.queryForList("SELECT * FROM socio ORDER BY numeroSocio ASC");
				rows.forEach(System. out ::println);
	}
	public Socio obtener(Socio id) {
		List<Map<String, Object>> rows = (List<Map<String, Object>>)
				jdbcTemplate.queryForList("SELECT * FROM socio WHERE numeroSocio = ?", id.getNumeroSocio());
				rows.forEach(System. out ::println);
				return id;
	}


			
}
