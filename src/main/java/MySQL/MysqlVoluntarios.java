package MySQL;

import java.io.File;

import java.util.List;
import java.util.Map;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;


import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;


import ClasesGenericas.Voluntario;
import ClasesGenericas.Voluntarios;
import DAO.VoluntarioDAO;



import Conexion.DataSourceJDBC;

public class MysqlVoluntarios implements VoluntarioDAO{
	
	
	private DataSourceJDBC mySqlDataSource = new DataSourceJDBC();
	private JdbcTemplate jdbcTemplate = new JdbcTemplate(mySqlDataSource.getDataSource());
	

	public void  insertar(Voluntario id)    {
		try {
	
		        File fileVoluntarios = new File("Voluntarios.xml");
		        JAXBContext context = JAXBContext. newInstance (Voluntarios.class);
			    Unmarshaller unmarshaller = context.createUnmarshaller();
			    Voluntarios voluntarios = (Voluntarios)unmarshaller.unmarshal(fileVoluntarios);
				
		        
		        List <Voluntario>datosvoluntarios = voluntarios.getVoluntarios();
		        
		        for(Voluntario voluntario :  datosvoluntarios) {
		        	if(id.getVoluntario_id() == voluntario.getVoluntario_id()) {
		        		jdbcTemplate.update("INSERT INTO voluntario VALUES(?,?,?,?,?,?,?,?)", id.getVoluntario_id(), id.getFechaIngreso(), id.getNombre(), id.getApellidos(), id.getDireccion(), id.getCorreoElectronico(), id.getPais(), id.getTipoDeVoluntario());
		        
				System.out.println("Se ha insertado en la base de datos el voluntario:" +voluntario.getVoluntario_id());
				
		        	}
		        }
		        
		        	
		        
	
		
	
		        	}catch(DataIntegrityViolationException esql){
		System. out .println("El voluntario " + id.getVoluntario_id() + " ya existe.");
		}
		catch(Exception exception) {
		System. out .println("* ERROR: " + exception.getMessage() + " *");
		}
		
	
		        


}
	@Override
	public void modificar(Voluntario id) {

		try {
			System.out.println(
					"---------- El Voluntario número: " + id.getVoluntario_id() + " ,va a ser modificado. ----------");

			String CFechaIngreso = jdbcTemplate.queryForObject(
					"SELECT fechaIngreso FROM voluntario WHERE numeroVoluntario = ?;", String.class,
					id.getVoluntario_id());
			String CNombre = jdbcTemplate.queryForObject("SELECT nombre FROM voluntario WHERE numeroVoluntario = ?;",
					String.class, id.getVoluntario_id());
			String CApellidos = jdbcTemplate.queryForObject(
					"SELECT apellidos FROM voluntario WHERE numeroVoluntario = ?;", String.class,
					id.getVoluntario_id());
			String CDireccion = jdbcTemplate.queryForObject(
					"SELECT direccion FROM voluntario WHERE numeroVoluntario = ?;", String.class,
					id.getVoluntario_id());
			String CCorreoElectronico = jdbcTemplate.queryForObject(
					"SELECT correoElectronico FROM voluntario WHERE numeroVoluntario = ?;", String.class,
					id.getVoluntario_id());
			String CPais = jdbcTemplate.queryForObject(
					"SELECT pais FROM voluntario WHERE numeroVoluntario = ?;", String.class, id.getVoluntario_id());
			String CTipoDeVoluntario = jdbcTemplate.queryForObject(
					"SELECT tipoDeVoluntario FROM voluntario WHERE numeroVoluntario = ?;", String.class,
					id.getVoluntario_id());
			
			if (!id.getFechaIngreso().equals(CFechaIngreso)) {
				jdbcTemplate.update("UPDATE voluntario SET fechaIngreso = ? WHERE numeroVoluntario = ?;",
						id.getFechaIngreso(), id.getVoluntario_id());
				System.out.println("---------- El Voluntario número: " + id.getVoluntario_id()
						+ " ha cambiado de fecha de Ingreso a: " + id.getFechaIngreso() + ". ----------");

			}
		
			if (!id.getNombre().equals(CNombre)) {
				jdbcTemplate.update("UPDATE voluntario SET nombre = ? WHERE numeroVoluntario = ?;",
						id.getNombre(), id.getVoluntario_id());
				System.out.println("---------- El Voluntario número: " + id.getVoluntario_id()
						+ " ha cambiado de nombre a: " + id.getNombre() + ". ----------");

			}

			if (!id.getApellidos().equals(CApellidos)) {
				jdbcTemplate.update("UPDATE voluntario SET apellidos = ? WHERE numeroVoluntario = ?;",
						id.getApellidos(), id.getVoluntario_id());
				System.out
						.println("---------- Los apellidos han cambiado a: " + id.getApellidos() + ". ----------");
			}
			if (!id.getDireccion().equals(CDireccion)) {
				jdbcTemplate.update("UPDATE voluntario SET direccion = ? WHERE numeroVoluntario = ?;", id.getDireccion(),
						id.getVoluntario_id());
				System.out.println("---------- La direccion del voluntario " + id.getVoluntario_id() + " ha cambiado a: "
						+ id.getDireccion() + ". ----------");
			}
			if (!id.getCorreoElectronico().equals(CCorreoElectronico)) {
				jdbcTemplate.update("UPDATE voluntario SET correoElectronico = ? WHERE numeroVoluntario = ?;", id.getCorreoElectronico(),
						id.getVoluntario_id());
				System.out.println("---------- El Correo electronico del voluntario " + id.getVoluntario_id() + " ha cambiado a: "
						+ id.getCorreoElectronico() + ". ----------");
			}
			if (!id.getPais().equals(CPais)) {
				jdbcTemplate.update("UPDATE voluntario SET pais = ? WHERE numeroVoluntario = ?;", id.getPais(),
						id.getVoluntario_id());
				System.out.println("---------- El Pais del proyecto " + id.getVoluntario_id() + " ha cambiado a: "
						+ id.getPais() + ". ----------");
			}
			
			if (!id.getTipoDeVoluntario().equals(CTipoDeVoluntario)) {
				jdbcTemplate.update("UPDATE voluntario SET tipoDeVoluntario = ? WHERE numeroVoluntario = ?;",
						id.getTipoDeVoluntario(), id.getVoluntario_id());
				System.out.println("---------- El tipo de Voluntario ha sido cambiado a: " + id.getTipoDeVoluntario()
						+ ". ----------");
			}


			System.out.println("---------- El Voluntario " + id.getVoluntario_id() + " ha sido modificado.---------\n ");

		} catch (DataIntegrityViolationException esql) {
			System.out.println("El proyecto " + id.getVoluntario_id() + " ya existe.");
		} catch (Exception exception) {
			System.out.println("* ERROR: " + exception.getMessage() + " *");
		}

	}


			
		
	@Override
	public void eliminar(Voluntario id) {
		
		jdbcTemplate.update("DELETE FROM  proyecto  WHERE codigoProyecto	= ?" ,id.getVoluntario_id());
        System. out .println("---------- El Proyecto " + id.getNombre()+ " se ha eliminado correctamente");
		
	}



	@Override
	public void obtenerTodos() {
		
		List<Map<String, Object>> rows = (List<Map<String, Object>>)
				jdbcTemplate.queryForList("SELECT * FROM voluntario ORDER BY numeroVoluntario ASC");
				rows.forEach(System. out ::println);
	}


	@Override
	public Voluntario obtener(Voluntario id) {
		List<Map<String, Object>> rows = (List<Map<String, Object>>)
				jdbcTemplate.queryForList("SELECT * FROM voluntario WHERE numeroVoluntario = ?", id.getVoluntario_id());
				rows.forEach(System. out ::println);
				return id;
	}







}