package MySQL;

import java.io.File;

import java.util.List;
import java.util.Map;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;


import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;


import ClasesGenericas.Proyecto;
import ClasesGenericas.Proyectos;
import DAO.ProyectosDAO;



import Conexion.DataSourceJDBC;

public class MysqlProyectos implements ProyectosDAO{
	
	
	private DataSourceJDBC mySqlDataSource = new DataSourceJDBC();
	private JdbcTemplate jdbcTemplate = new JdbcTemplate(mySqlDataSource.getDataSource());
	

	public void  insertar(Proyecto id)    {
		try {
	
		        File fileProyectos = new File("Proyectos.xml");
		        JAXBContext context = JAXBContext. newInstance (Proyectos.class);
			    Unmarshaller unmarshaller = context.createUnmarshaller();
			    Proyectos proyectos = (Proyectos)unmarshaller.unmarshal(fileProyectos);
				
		        
		        List <Proyecto>datosproyectos = proyectos.getProyectos();
		        
		        for(Proyecto proyecto : datosproyectos) {
		        	if(id.getCodigoProyecto() == proyecto.getCodigoProyecto()) {
		        		jdbcTemplate.update("INSERT INTO proyecto VALUES(?,?,?,?,?,?,?,?,?)", id.getCodigoProyecto(),id.getNombreProyecto(),id.getPais(),id.getLocalizacion(),id.getFechaInicio(),id.getFechaFinalizacion(),id.getSocioLocal(),id.getFinanciador(),id.getFinanciacionAportada());
		        
				System.out.println("Se ha insertado en la base de datos el proyecto:" +proyecto.getCodigoProyecto());
				
		        	}
		        }
		        
		        	
		        
	
		
	
		        	}catch(DataIntegrityViolationException esql){
		System. out .println("El proyecto " + id.getNombreProyecto() + " ya existe.");
		}
		catch(Exception exception) {
		System. out .println("* ERROR: " + exception.getMessage() + " *");
		}
		
	
		        


}
	@Override
	public void modificar(Proyecto id) {

		try {
			System.out.println(
					"---------- El Proyecto número: " + id.getCodigoProyecto() + " ,va a ser modificado. ----------");

			String CNombre = jdbcTemplate.queryForObject(
					"SELECT nombreProyecto FROM proyectos WHERE codigoProyecto = ?;", String.class,
					id.getCodigoProyecto());
			String CPais = jdbcTemplate.queryForObject("SELECT pais FROM proyectos WHERE codigoProyecto = ?;",
					String.class, id.getCodigoProyecto());
			String CLocalizacion = jdbcTemplate.queryForObject(
					"SELECT localizacion FROM proyectos WHERE codigoProyecto = ?;", String.class,
					id.getCodigoProyecto());
			String CFechaInicio = jdbcTemplate.queryForObject(
					"SELECT fechaInicio FROM proyectos WHERE codigoProyecto = ?;", String.class,
					id.getCodigoProyecto());
			String CFechaFinalizacion = jdbcTemplate.queryForObject(
					"SELECT fechaFinalizacion FROM proyectos WHERE codigoProyecto = ?;", String.class,
					id.getCodigoProyecto());
			String CSocioLocal = jdbcTemplate.queryForObject(
					"SELECT socioLocal FROM proyectos WHERE codigoProyecto = ?;", String.class, id.getCodigoProyecto());
			String CFinanciador = jdbcTemplate.queryForObject(
					"SELECT financiador FROM proyectos WHERE codigoProyecto = ?;", String.class,
					id.getCodigoProyecto());
			Double CFinanciacionAportada = jdbcTemplate.queryForObject(
					"SELECT financiacionAportada FROM proyectos WHERE codigoProyecto = ?;", Double.class,
					id.getCodigoProyecto());

			if (!id.getNombreProyecto().equals(CNombre)) {
				jdbcTemplate.update("UPDATE proyectos SET nombreProyecto = ? WHERE codigoproyecto = ?;",
						id.getNombreProyecto(), id.getCodigoProyecto());
				System.out.println("---------- El Proyecto número: " + id.getCodigoProyecto()
						+ " ha cambiado de nombre a: " + id.getNombreProyecto() + ". ----------");

			}

			if (!id.getPais().equals(CPais)) {
				jdbcTemplate.update("UPDATE proyectos SET pais = ? WHERE codigoproyecto = ?;", id.getPais(),
						id.getCodigoProyecto());
				System.out.println("---------- El Pais del proyecto " + id.getCodigoProyecto() + " ha cambiado a: "
						+ id.getPais() + ". ----------");
			}

			if (!id.getLocalizacion().equals(CLocalizacion)) {
				jdbcTemplate.update("UPDATE proyectos SET localizacion = ? WHERE codigoproyecto = ?;",
						id.getLocalizacion(), id.getCodigoProyecto());
				System.out
						.println("---------- La localizacion ha cambiado a: " + id.getLocalizacion() + ". ----------");
			}

			if (!id.getFechaInicio().equals(CFechaInicio)) {
				jdbcTemplate.update("UPDATE proyectos SET fechaInicio = ? WHERE codigoproyecto = ?;",
						id.getFechaInicio(), id.getCodigoProyecto());
				System.out.println("---------- La fecha de inicio ha sido cambiada a : " + id.getFechaFinalizacion()
						+ ". ----------");
			}

			if (!id.getFechaFinalizacion().equals(CFechaFinalizacion)) {
				jdbcTemplate.update("UPDATE proyectos SET fechaFinalizacion = ? WHERE codigoproyecto = ?;",
						id.getFechaFinalizacion(), id.getCodigoProyecto());
				System.out.println("---------- La fecha cde finalización ha sido cambiada a : "
						+ id.getFechaFinalizacion() + ". ----------");
			}

			if (!id.getSocioLocal().equals(CSocioLocal)) {
				jdbcTemplate.update("UPDATE proyectos SET socioLocal = ? WHERE codigoproyecto = ?;", id.getSocioLocal(),
						id.getCodigoProyecto());
				System.out.println("---------- El socio local ha cambiado a: " + id.getSocioLocal() + ". ----------");
			}

			if (!id.getFinanciador().equals(CFinanciador)) {
				jdbcTemplate.update("UPDATE proyectos SET financiador = ? WHERE codigoproyecto = ?;",
						id.getFinanciador(), id.getCodigoProyecto());
				System.out.println("---------- El financiador ha cambiado a: " + id.getFinanciador() + ". ----------");
			}

			if (!id.getFinanciacionAportada().equals(CFinanciacionAportada)) {
				jdbcTemplate.update("UPDATE proyectos SET financiacionAportada = ? WHERE codigoproyecto = ?;",
						id.getFinanciacionAportada(), id.getCodigoProyecto());
				System.out.println("---------- La Financiación aportada ha cambiado: " + id.getFinanciacionAportada()
						+ ". ----------");
			}

			System.out.println("---------- El Proyecto " + id.getCodigoProyecto() + " ha sido modificado.---------\n ");

		} catch (DataIntegrityViolationException esql) {
			System.out.println("El proyecto " + id.getNombreProyecto() + " ya existe.");
		} catch (Exception exception) {
			System.out.println("* ERROR: " + exception.getMessage() + " *");
		}

	}


			
		
		
	



	@Override
	public void eliminar(Proyecto id) {
		
		jdbcTemplate.update("DELETE FROM  proyecto  WHERE codigoProyecto	= ?" ,id.getCodigoProyecto());
        System. out .println("---------- El Proyecto " + id.getNombreProyecto()+ " se ha eliminado correctamente");
		
	}



	@Override
	public void obtenerTodos() {
		
		List<Map<String, Object>> rows = (List<Map<String, Object>>)
				jdbcTemplate.queryForList("SELECT * FROM proyecto ORDER BY codigoProyecto ASC");
				rows.forEach(System. out ::println);
	}


	@Override
	public Proyecto obtener(Proyecto id) {
		List<Map<String, Object>> rows = (List<Map<String, Object>>)
				jdbcTemplate.queryForList("SELECT * FROM proyecto WHERE codigoProyecto = ?", id.getCodigoProyecto());
				rows.forEach(System. out ::println);
				return id;
	}







}