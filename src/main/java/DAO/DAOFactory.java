package DAO;

import javax.xml.bind.JAXBException;

import MySQL.MysqlProyectos;
import MySQL.MysqlSocios;
import MySQL.MysqlVoluntarios;

public abstract class DAOFactory {

	public static final int XML = 1;
	public static final int MYSQL = 2;
	
	public abstract MysqlProyectos getProyectosDao() throws JAXBException;
	public abstract MysqlSocios getSociosDao() throws JAXBException;
	public abstract MysqlVoluntarios getVoluntariosDao()throws JAXBException;
	
	public abstract XmlProyectos getProyectos()throws JAXBException;
	public abstract XmlSocios getSocios()throws JAXBException;
	public abstract XmlVoluntarios getVoluntarios()throws JAXBException;
	
	public static DAOFactory getDAOFactory(int whichFactory) {

		switch (whichFactory) {
		case XML:
			return new XmlDAOFactory();
			
	    case MYSQL:
			 return (DAOFactory) new MysqlDAOFactory();
			 
	    default:
	    	return null;
		
			
		}
	}
}