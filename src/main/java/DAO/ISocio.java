package DAO;
import javax.xml.bind.JAXBException;

import ClasesGenericas.Socios;


public interface ISocio {
	public void guardarSocios(Socios socios) throws JAXBException;
	public Socios listarSocios() throws JAXBException;
	
	

}

