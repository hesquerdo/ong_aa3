package DAO;
import javax.xml.bind.JAXBException;

import ClasesGenericas.Proyectos;


public interface IProyectos {
	public void guardarProyectos(Proyectos proyectos) throws JAXBException;
	public Proyectos listarProyectos() throws JAXBException;

}
