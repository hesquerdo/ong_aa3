package DAO;

import java.io.File;  


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


import ClasesGenericas.Socios;
import ClasesGenericas.Socio;

public class XmlSocios implements ISocio {
	
	private JAXBContext jaxbContext = null;
	private String nombreFichero = null;
	
	public XmlSocios ()throws JAXBException{
		this.jaxbContext = JAXBContext.newInstance(Socios.class);
		this.nombreFichero = "Socios.xml";
	}

	public void guardarSocios(Socios socios) throws JAXBException {
	Marshaller marshaller = jaxbContext.createMarshaller();
	marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	marshaller.marshal(socios, new File(nombreFichero));
	System.out.println();
	System.out.println("Se ha escrito el fichero" + nombreFichero + "con el siguiente contenido");
	System.out.println();
	marshaller.marshal(socios, System.out);
	
		
	}

	@Override
	public Socios listarSocios() throws JAXBException {
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		Socios socios = (Socios) unmarshaller.unmarshal(new File(nombreFichero));
		System.out.println();
		System.out.println("Estos son los socios que tenemos en el fichero"+ nombreFichero);
		
		for(Socio socio : socios.getSocios()) 
		{
			System.out.println("-------------------------------------------------------------");
			System.out.println("Numero de socio: \t"+socio.getNumeroSocio());
			System.out.println("Fecha de ingreso: \t"+socio.getFechaIngreso());
			System.out.println("Tipo de cuota: \t"+socio.getTipoCuota());
			System.out.println("Nombre: \t"+socio.getNombre());
			System.out.println("Apellidos: \t"+ socio.getApellidos());
			System.out.println("Dirección: \t"+socio.getDireccion());
			System.out.println("Correo Elecgtronico: \t"+socio.getCorreoElectronico());
			System.out.println("-------------------------------------------------------------");
			
			
		}
		
		return null;
	}

	
}
