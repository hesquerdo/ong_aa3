package DAO;

public interface DAOGenericoMysql<Clase, id> {
	
	void insertar (Clase id) ;
	
	void modificar (Clase id);
	
	void eliminar (Clase id);
	
	void obtenerTodos ();
	
	Clase obtener (Clase id);

	
}